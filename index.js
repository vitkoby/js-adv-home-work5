
class Card {
	constructor(post, user) {
		this.post = post;
		this.user = user;
		this.element = this.createCardElement();
	}

	
	createCardElement() {
		const card = document.createElement("div");
		card.classList.add("card");

		const heading = document.createElement("h2");
		heading.innerText = this.post.title;
		card.appendChild(heading);

		const text = document.createElement("p");
		text.innerText = this.post.body;
		card.appendChild(text);

		const author = document.createElement("p");
		author.innerHTML = `Author: ${this.user.name} ${this.user.surname} (${this.user.email})`;
		card.appendChild(author);

		const deleteButton = document.createElement("button");
		deleteButton.innerHTML = "&#x2715;";
		deleteButton.addEventListener("click", () => {
			this.deleteCard();
		});
		card.appendChild(deleteButton);

		return card;
	}

	
	deleteCard() {
		fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
			method: "DELETE",
		})
			.then((response) => {
				if (response.ok) {
					this.element.remove();
				} else {
					console.error(`Error deleting post ${this.post.id}: ${response.status}`);
				}
			})
			.catch((error) => {
				console.error(`Error deleting post ${this.post.id}: ${error}`);
			});
	}
}

Promise.all([
	fetch("https://ajax.test-danit.com/api/json/users"),
	fetch("https://ajax.test-danit.com/api/json/posts"),
])
	.then((responses) => {
		return Promise.all(responses.map((response) => response.json()));
	})
	.then(([users, posts]) => {
		const cards = posts.map((post) => {
			const user = users.find((user) => user.id === post.userId);
			return new Card(post, user);
		});

		const container = document.querySelector("#cards-container");
		cards.forEach((card) => {
			container.appendChild(card.element);
		});
	})
	.catch((error) => {
		console.error(`Error loading data from server: ${error}`);
	});
